package com.miser.runapp;

import android.app.VoiceInteractor;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Intent intent = getIntent();
        String runStatus = intent.getStringExtra("actionStatus");

        Toast.makeText(this, "Status: " + runStatus, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (!isVoiceInteraction()) {
            return;
        }

        VoiceInteractor.PickOptionRequest.Option basicRunOption
                = new VoiceInteractor.PickOptionRequest.Option("Basic Run", 0);
        basicRunOption.addSynonym("Basic");

        VoiceInteractor.PickOptionRequest.Option speedRunOption
                = new VoiceInteractor.PickOptionRequest.Option("Speed Run", 1);
        speedRunOption.addSynonym("Speed");

        VoiceInteractor.Prompt prompt = new VoiceInteractor.Prompt("What type of a run today?");
        getVoiceInteractor().submitRequest(new VoiceInteractor.PickOptionRequest(prompt,
                        new VoiceInteractor.PickOptionRequest.Option[]{basicRunOption,
                                speedRunOption}, null) {
                    @Override
                    public void onPickOptionResult(boolean finished, Option[] selections,
                            Bundle result) {
                        if (finished && selections.length == 1) {
                            if (selections[0].getIndex() == 0) {
                                Toast.makeText(MainActivity.this, "Selected: Basic Run", Toast.LENGTH_SHORT).show();
                            } else if (selections[0].getIndex() == 1) {
                                Toast.makeText(MainActivity.this, "Selected: Speed Run", Toast.LENGTH_SHORT).show();
                            }
                        } else {

                        }
                    }

                    @Override
                    public void onCancel() {
                    }
                });
    }
}
